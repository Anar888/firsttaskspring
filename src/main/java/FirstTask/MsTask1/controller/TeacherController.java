package FirstTask.MsTask1.controller;

import FirstTask.MsTask1.service.TeacherService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacher")
public class TeacherController {

    private final TeacherService teacherService;
    public TeacherController(TeacherService teacherService){
        this.teacherService=teacherService;
    }

    @PostMapping
    public void create(@RequestBody Teacher teacher){
        teacherService.create(teacher);
    }
    @GetMapping("/{id}")
    public Teacher get(@PathVariable int id){
        return teacherService.get(id);
    }
    @PutMapping
    public void update(@RequestBody Teacher teacher){
        teacherService.update(teacher);
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id){
        teacherService.delete(id);
    }
    @GetMapping
    public List<Teacher> getAll(){
        return teacherService.getAll();
    }
}
