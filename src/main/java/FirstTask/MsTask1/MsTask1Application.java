package FirstTask.MsTask1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsTask1Application {

	public static void main(String[] args) {
		SpringApplication.run(MsTask1Application.class, args);
	}

}
