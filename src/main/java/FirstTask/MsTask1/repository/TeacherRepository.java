package FirstTask.MsTask1.repository;

import FirstTask.MsTask1.controller.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<Teacher,Integer> {
}
